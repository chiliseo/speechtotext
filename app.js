/**
 * Copyright 2015 IBM Corp. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var express = require('express'),
  app = express(),
  vcapServices = require('vcap_services'),
  extend = require('util')._extend,
  watson = require('watson-developer-cloud');
var expressBrowserify = require('express-browserify');

// load environment properties from a .env file for local development
require('dotenv').load({silent: true});

// Bootstrap application settings
require('./config/express')(app);

// automatically compile and serve the front-end js
app.get('/js/index.js', expressBrowserify('src/index.js', {
  watch: process.env.NODE_ENV !== 'production'
}));


// For local development, replace username and password
var textToSpeech = watson.text_to_speech({
  version: 'v1',
  username: 'a85d3e94-47db-47b4-a542-b4e2ab7e5ec9',
  password: 'kX4cFstFTGca'
});

app.get('/api/synthesize', function(req, res, next) {
  var transcript = textToSpeech.synthesize(req.query);
  transcript.on('response', function(response) {
    if (req.query.download) {
      response.headers['content-disposition'] = 'attachment; filename=transcript.ogg';
    }
  });
  transcript.on('error', function(error) {
    next(error);
  });
  transcript.pipe(res);
});

// For local development, replace username and password
var config = extend({
  version: 'v1',
  url: 'https://stream.watsonplatform.net/speech-to-text/api/v1/sessions?model=en-US_BroadbandModel',
  username: '30513731-2798-4186-8a14-efae83884c09',
  password: 'MFdXXTHa51MR'
}, vcapServices.getCredentials('speech_to_text'));

// var config = extend({
//   version: 'v1',
//   url: 'https://stream.watsonplatform.net/speech-to-text/api',
//   username: process.env.STT_USERNAME || '30513731-2798-4186-8a14-efae83884c09',
//   password: process.env.STT_PASSWORD || 'MFdXXTHa51MR'
// }, vcapServices.getCredentials('speech_to_text'));




var authService = watson.authorization(config);

app.get('/', function(req, res) {
  res.render('index', {
    ct: req._csrfToken,
    GOOGLE_ANALYTICS_ID: process.env.GOOGLE_ANALYTICS_ID
  });
});

// Get token using your credentials
app.post('/api/token', function(req, res, next) {
  authService.getToken({url: config.url}, function(err, token) {
    if (err)
      next(err);
    else
      res.send(token);
  });
});

// error-handler settings
require('./config/error-handler')(app);

module.exports = app;
